import java.util.Scanner;

class GameofThree {
    public static void main(String[] args) {
    	int num = getInputNum();
        gameOfThree(num);  
    }
    public static int getInputNum() {
        // gets and returns an integer input greater than three
        Scanner userInput = new Scanner(System.in);
        int num=0;
        boolean validInt=false;
        while (!validInt) {
            if (userInput.hasNextInt()) {
                num = userInput.nextInt();
                if ( num < 1 ) {
                    System.out.println ("Please pick a number greater than 1");
                }
                else {
                    validInt=true;       
                }
            }
            else {
                System.out.println("Please pick an integer");
            }
           userInput.nextLine(); // clear for next input
        }
        return num;
    }
    public static void gameOfThree(int num) {
        // Plays the game of three with the number, displaying the process
        while (num != 1) {
            int remainder = num % 3;
            int additionNumber = 0;
            if (remainder == 2) {
                additionNumber = 1;
            }
            else if (remainder == 1) {
                additionNumber = -1;
            }
            System.out.println (num + " " + additionNumber);
            num += additionNumber;
            num /= 3;
        }
        System.out.println (num);
    }
}