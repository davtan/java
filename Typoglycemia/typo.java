import java.util.Scanner;
import java.util.Random;

class Typo {
	public static void main(String[] args) {
		String input = getInput ();
		String typoInput = scrambleSentence(input);
		System.out.println(typoInput);
	}
	public static String getInput() {
		Scanner userInput = new Scanner(System.in);
		return userInput.nextLine();
	}
	public static String scrambleSentence(String sentence) {
		// scrambles the words in the sentence with the first and last letter intact
		String[] words = sentence.split ("\\s+");
		String newSentence="";
		for (int i = 0; i < words.length; i++) {
			String word = scrambleWord(words[i]);
			newSentence+= word + " ";	
		}
		return newSentence;
	}
	public static String scrambleWord(String word) {
		char letters[] = word.toCharArray();
		int endLetterIndex = letters.length-1;
		while (!Character.isLetter(letters[endLetterIndex])) {
			--endLetterIndex;
			if (endLetterIndex==-1) {
				// this word is just punctuation
				break;
			}
		}
		if (endLetterIndex <= 2) {
			// words that is three letters or less do not need to be scrambled
			return word;
		}
		Random random = new Random ();
		int endSwitchableIndex = endLetterIndex-1;
		for (int i = 1; i < endSwitchableIndex; i++) {
			int nextLetter = random.nextInt(endSwitchableIndex) + 1; // guarenteed to be at least 1
			char tempLetter = letters[i];
			letters[i] = letters[nextLetter];
			letters[nextLetter] = tempLetter;
		}
		return new String ( letters );
	}
	public static String scrambleParagraph (String paragraph) {
		String[] sentences = paragraph.split("\n");
		String newParagraph ="";
		for (int i = 0; i < sentences.length;i++) {
			String sentence = scrambleSentence(sentences[i]);
			newParagraph+=sentence + "\n";
		}
		return newParagraph;
	}
}