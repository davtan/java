import java.util.Scanner;
import java.util.Stack;

class ZeroSum {
    static Stack<String> printStack = new Stack<String>();

    public static void main(String[] args) {
        int num = getInputNum();
        if (!solveZeroSum(num,0)) {
            System.out.println("Impossible");
        }
        else {
            while (true) {
                String print = printStack.pop ();
                System.out.println (print);
                if (print == "1") {
                    break;
                }
            }
        }
    }
    public static int getInputNum() {
        // gets and returns an integer input greater than three
        Scanner userInput = new Scanner(System.in);
        int num=0;
        boolean validInt=false;
        while (!validInt) {
            if (userInput.hasNextInt()) {
                num = userInput.nextInt();
                if ( num < 1 ) {
                    System.out.println ("Please pick a number greater than 1");
                }
                else {
                    validInt=true;       
                }
            }
            else {
                System.out.println("Please pick an integer");
            }
           userInput.nextLine(); // clear for next input
        }
        return num;
    }
    public static boolean solveZeroSum(int num,int totalSum) {
        if (num < 1 ) {
            return false;
        }
        if (num == 1) {
            if ( totalSum == 0) {
                printStack.push ("1");
                return true;
            }
            else {
                return false;
            }
        }
        int remainder = num % 3;
        int negativeAdditiveNum = remainder * -1;
        int additiveNum = 3 - remainder;
        if (solveZeroSum( ( num+negativeAdditiveNum ) /3,totalSum+negativeAdditiveNum)) {
            printStack.push ( num + " " + negativeAdditiveNum );
            return true;
        }
        else if (remainder != 0) {  // takes care when remainder is 0
            if ( solveZeroSum( ( num+additiveNum )/3 , totalSum+additiveNum) ) {
                printStack.push ( num + " " + additiveNum);
                return true;
            }
        }   
        return false;
    }
}