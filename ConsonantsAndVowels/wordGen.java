import java.util.Scanner;
import java.util.Random;

class WordGenerator {
	
	public static void main(String[] args) {
		char[] lowerVowels = {'a','e','i','o','u'};
		char[] upperVowels = {'A','E','I','O','U'};
		char[] lowerConsonants = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z'};
		char[] upperConsonants = {'B','C','D','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z'};
				
		char[] input = getInput ().toCharArray();
		if (input.length!=0) {
			input = replaceChars(input,'v',lowerVowels);
			input = replaceChars(input,'c',lowerConsonants);
			input = replaceChars(input,'V',upperVowels);
			input = replaceChars(input,'C',upperConsonants);

			System.out.println(input);
		}
	}
	public static char[] replaceChars(char[] placeholderChars,char placeholder,char[] replacementChars) {
		// replaces the placeholderChars that match the placeholder with a random char from replacementChars
		Random random = new Random ();
		for (int i=0;i<placeholderChars.length;++i) {
			if (placeholderChars[i]==placeholder) {
				placeholderChars[i]=replacementChars[random.nextInt(replacementChars.length-1)];
			}
		}
		return placeholderChars;
	}
	public static String getInput() {
		Scanner userInput = new Scanner(System.in);
		return userInput.nextLine();
	}

}